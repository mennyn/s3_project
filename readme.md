# Embedded AI project

## Description:
This project consists of an AI embedded onto a STM32 chip mounted on a Nucleo card.  
The card is then connected to different electronic devices via its ports.  
The AI is designed to recognise the starting voltage curve of the devices which allows it to tell if the given device is functional or dysfonctional.

## Members:

Lisa Guezenec   
Sophie Scheidt   
Antoine Tartare   
Nathan Menny

## additional information:

This project was created during a universitary project at the *Institute of Technology of Valence*.  
And has been made for the *Cari Electronic* company.