import csv
import os
import re
import random

# The functional cards curves files 
inputFolder = os.listdir("./courbes_cartes/functional/")

# Create a csv file
def createCSV(file, time, data) :
	ch = "x-axis,1\nsecond,Volt\n"

	for i in range(len(time)) : 
		ch += time[i] + "," +data[i] +"\n"
	

	save = open("./courbes_cartes/dysfunctional/" + file, "w")
	save.write(ch)
	save.close()


for file in inputFolder : 

	CSVFile = open("./courbes_cartes/functional/" + file, "r")

	csvFileReader = csv.reader(CSVFile)
	csvFileReader.__next__() # Skip first line (x-axis,1)
	csvFileReader.__next__() # Skip second line (seconds, Volt)

	inputData = [] # Stores de voltage values
	time = [] # Stores the time in seconds

	for line in csvFileReader : 
		if len(line) == 2 :
			if line[1] != '' :
				time.append(line[0])
				inputData.append(line[1])
	
	# Shuffles the input
	random.shuffle(inputData)


	createCSV(file, time, inputData)



