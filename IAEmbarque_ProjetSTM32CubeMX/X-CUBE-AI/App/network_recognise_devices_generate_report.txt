Neural Network Tools for STM32 v1.2.0 (AI tools v5.0.0)
Created date       : 2021-03-30 17:28:29

Exec/report summary (generate dur=3.990s err=0)
------------------------------------------------------------------------------------------------------------------------
model file         : D:\IUT\_Projet_S3\pyfiles\voltGraphLearning\identify_board\reseau.tflite
type               : tflite (tflite)
c_name             : network_recognise_devices
compression        : 8
quantize           : None
L2r error          : NOT EVALUATED
workspace dir      : C:\Users\nathan\AppData\Local\Temp\mxAI_workspace468856457446800708349409508596285
output dir         : C:\Users\nathan\.stm32cubemx\stm32ai_output

model_name         : reseau
model_hash         : 09945ed51380b3b904583926d9b0b29c
input              : input_0 [100 items, 400 B, ai_float, FLOAT32, (100,)]
input (total)      : 400 B
output             : nl_3 [3 items, 12 B, ai_float, FLOAT32, (3,)]
output (total)     : 12 B
params #           : 598 items (2.34 KiB)
macc               : 640
weights (ro)       : 604 (604 B) (-74.75%) 
activations (rw)   : 60 (60 B) 
ram (total)        : 472 (472 B) = 60 + 400 + 12

------------------------------------------------------------------------------------------------------------------------
id  layer (type)        output shape      param #     connected to             macc           rom                
------------------------------------------------------------------------------------------------------------------------
0   input_0 (Input)     (100,)                                                                                   
    dense_0 (Dense)     (5,)              505         input_0                  500            336 (c)            
    nl_0 (Nonlinearity) (5,)                          dense_0                  5                                 
------------------------------------------------------------------------------------------------------------------------
1   dense_1 (Dense)     (10,)             60          nl_0                     50             136 (c)            
    nl_1 (Nonlinearity) (10,)                         dense_1                  10                                
------------------------------------------------------------------------------------------------------------------------
2   dense_2 (Dense)     (3,)              33          nl_1                     30             132                
------------------------------------------------------------------------------------------------------------------------
3   nl_3 (Nonlinearity) (3,)                          dense_2                  45                                
------------------------------------------------------------------------------------------------------------------------
reseau p=598(2.34 KBytes) macc=640 rom=604 Bytes (-74.75%) ram=60 B io_ram=412 B

 
Complexity per-layer - macc=640 rom=604
------------------------------------------------------------------------------------------------------------------------
id      layer (type)        macc                                    rom                                    
------------------------------------------------------------------------------------------------------------------------
0       dense_0 (Dense)     |||||||||||||||||||||||||||||||  78.1%  |||||||||||||||||||||||||||||||  55.6% 
0       nl_0 (Nonlinearity) |                                 0.8%  |                                 0.0% 
1       dense_1 (Dense)     ||||                              7.8%  |||||||||||||                    22.5% 
1       nl_1 (Nonlinearity) |                                 1.6%  |                                 0.0% 
2       dense_2 (Dense)     ||                                4.7%  ||||||||||||                     21.9% 
3       nl_3 (Nonlinearity) |||                               7.0%  |                                 0.0% 
------------------------------------------------------------------------------------------------------------------------


Evaluation report (summary)
--------------------------------------------------
NOT EVALUATED
